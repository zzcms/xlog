// sysinfo
package http

import (
	"io/ioutil"
	"strings"
)

func readload() string {
	fi, err := ioutil.ReadFile("/proc/loadavg")
	checkError(err)
	ldnow := strings.Split(string(fi), " ")
	return ldnow[0]
}
