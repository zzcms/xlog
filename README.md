### 安装方法：
> 1.将src/xlog/xlog 文件放入/usr/bin,然后给予执行权限： `chmod +x /usr/bin/xlog`

> 2.将xlog.cfg 放入/etc/下

> 3.将qqwry.dat 放入当前目录

然后执行xlog -h 有提升帮助信息就是安装成功

![demon](https://gitee.com/524831546/xlog/raw/master/xlog%20demo/xlog%20demo.png "首页")
![状态码](https://gitee.com/524831546/xlog/raw/master/xlog%20demo/xlog%20statscode.png "状态码")
![运营商](https://gitee.com/524831546/xlog/raw/master/xlog%20demo/xlog%20netOperator.png "运营商")
![ip](https://gitee.com/524831546/xlog/raw/master/xlog%20demo/xlog%20IP.png "ip")
![useragent](https://gitee.com/524831546/xlog/raw/master/xlog%20demo/xlog%20useragent.png "浏览器")
![地图](https://gitee.com/524831546/xlog/raw/master/xlog%20demo/xlog%20chinamap.png "地图")